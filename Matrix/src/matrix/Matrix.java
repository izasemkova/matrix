package matrix;

import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * This class gets matrix from user(size and values), finds columns with minimal
 * values and deletes them.
 *
 * @version 1.0 26 March 2016
 * @author Irina Zasemkova
 */
public class Matrix {

    public static void main(String[] args) {

        int[][] initialMatrix = createMatrix();

        System.out.println("Начальная матрица:");
        printMatrix(initialMatrix);

        int[][] resultMatrix = transformMatrix(initialMatrix);

        System.out.println("Преобразованная матрица:");
        printMatrix(resultMatrix);

    }

    private static int[][] createMatrix() {

        int rowAmount, columnAmount;
        String input;

        Scanner in = new Scanner(System.in);

        System.out.println("Введите размерность матрицы.");

        while (true) {
            System.out.println("Введите количество строк:");
            input = in.next();
            try {
                rowAmount = Integer.parseInt(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Количество строк должно быть числом.");
            }
        }

        while (true) {
            System.out.println("Введите количество столбцов:");
            input = in.next();
            try {
                columnAmount = Integer.parseInt(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Количество столбцов должно быть числом.");
            }
        }

        int[][] matrix = new int[rowAmount][columnAmount];

        for (int i = 1; i <= rowAmount; i++) {
            System.out.println("Введите элементы " + i + " строки.");
            for (int j = 1; j <= columnAmount; j++) {
                while (true) {
                    input = in.next();
                    try {
                        matrix[i - 1][j - 1] = Integer.parseInt(input);
                        break;
                    } catch (NumberFormatException e) {
                        System.out.println("Вы ошиблись и ввели не число. Попробуйте еще раз.");
                    }
                }
            }
        }

        return matrix;
    }

    private static void printMatrix(int[][] initialMatrix) {
        for (int i = 0; i < initialMatrix.length; i++) {
            for (int j = 0; j < initialMatrix[i].length; j++) {
                System.out.print(initialMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static int[][] transformMatrix(int[][] initialMatrix) {

        SortedSet<Integer> minNumbersPositions = new TreeSet<>();
        int min = initialMatrix[0][0];
        minNumbersPositions.add(0);

        for (int i = 0; i < initialMatrix.length; i++) {
            for (int j = 0; j < initialMatrix[0].length; j++) {
                if (initialMatrix[i][j] == min) {
                    minNumbersPositions.add(j);
                } else if (initialMatrix[i][j] < min) {
                    min = initialMatrix[i][j];
                    minNumbersPositions.clear();
                    minNumbersPositions.add(j);
                }
            }
        }

        int rowAmount = initialMatrix.length;
        int columnAmount = initialMatrix[0].length - minNumbersPositions.size();

        int[][] matrix = new int[rowAmount][columnAmount];

        int k = 0;
        for (int j = 0; j < initialMatrix[0].length; j++) {
            if (!minNumbersPositions.contains(j)) {
                for (int i = 0; i < initialMatrix.length; i++) {
                    matrix[i][k] = initialMatrix[i][j];
                }
                k++;
            }
        }

        return matrix;
    }

}
