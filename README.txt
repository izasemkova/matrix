Консольное приложение Matrix реализовано в среде NetBeans 8.1 с использованием JDK 1.7.

Приложение получает на входе матрицу чисел M на N (размерность матрицы и ее элементы вводятся пользователем с консоли), 
отображает данную матрицу на экране, затем удаляет все столбцы с минимальным числом и результат выводит на экран

Для запуска проекта из командной строки, необходимо перейти в папку, где находится файл Matrix.jar и
выполнить следующую команду:

java -jar Matrix.jar 

